package me.scholtes.customitems.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class TabCompletion implements TabCompleter {

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String labels, String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("customitem") && args.length == 1) {
			File data = new File("plugins/CustomItems/", "itemnames.yml");
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
			
			if (!(sender instanceof Player)) {
				return null;
			}
			
			if (cfg.getStringList("items") == null) {
				return null;
			}
			
			List<String> completion = new ArrayList<String>();
			for (String s : cfg.getStringList("items")) {
	            String[] part = s.split(";");
	            completion.add(part[0]);
			}
			
			return completion;
		}
		
		return null;
	}

}
