package me.scholtes.customitems.commands;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.scholtes.customitems.Utils;

public class GiveCommand implements CommandExecutor {
	
	private Utils utils;
	
	public GiveCommand(Utils utils) {
		this.utils = utils;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.isOp()) {
			sender.sendMessage(utils.color("&cYou can't do this!"));
			return true;
		}
		if (args.length == 0) {
			sender.sendMessage(utils.color("&cCorrect usage: /customitem <item> <tier> <player>!"));
			return true;
		}
		if (args.length == 1) {
			sender.sendMessage(utils.color("&cCorrect usage: /customitem <item> <tier> <player>!"));
			return true;
		}
		if (args.length == 2) {
			sender.sendMessage(utils.color("&cCorrect usage: /customitem <item> <tier> <player>!"));
			return true;
		}
		if (args.length == 3) {
			File data = new File("plugins/CustomItems/", "itemnames.yml");
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
			int maxTier = 1;
			int tier = Integer.parseInt(args[1]);
			boolean isItem = false;
			for (String s : cfg.getStringList("items")) {
	            String[] part = s.split(";");
	            if (!part[0].equals(args[0])) {
	            	continue;
	            }
	            isItem = true;
	            maxTier = Integer.parseInt(part[1]);
			}
			if (!isItem) {
				sender.sendMessage(utils.color("&cThat's not a valid item!"));
				return true;
			}
			if (tier < 0 || tier > maxTier) {
				sender.sendMessage(utils.color("&cThat's not a valid tier!"));
				return true;
			}
			if (Bukkit.getPlayer(args[2]) == null || !Bukkit.getPlayer(args[2]).isOnline()) {
				sender.sendMessage(utils.color("&cThat's not a valid player!"));
				return true;
			}
			Bukkit.getPlayer(args[2]).getInventory().addItem(utils.giveItem(args[0].toLowerCase(), tier));
			sender.sendMessage(utils.color("&6Item &8 > &fYou have successfully given the item!"));
			Bukkit.getPlayer(args[2]).sendMessage(utils.color("&6Item &8 > &fYou have received a custom item!"));
			return true;
		}
		return true;
	}

}
