package me.scholtes.customitems.effects;

import java.io.File;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.scholtes.customitems.Utils;

public class ArmourPiercer implements Listener {
	
	private Utils utils;

	public ArmourPiercer(Utils utils) {
		this.utils = utils;
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!p.isSneaking()) {
			return;
		}
		if (e.getAction() != Action.LEFT_CLICK_AIR) {
			return;
		}
		if (p.getInventory().getItem(8) == null) {
			return;
		}
		
		if (p.getInventory().getItemInMainHand() == null) return;
		
		ItemStack mainHand = p.getInventory().getItemInMainHand();
		
		if (mainHand.getType() != Material.DIAMOND_SWORD &&
			mainHand.getType() != Material.IRON_SWORD &&
			mainHand.getType() != Material.GOLD_SWORD &&
			mainHand.getType() != Material.STONE_SWORD &&
			mainHand.getType() != Material.WOOD_SWORD) return;
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("armourpiercer", 1))) {
			if (utils.toggleState(p, "armourpiercer", 1)) {
				utils.applyLore("&7Piercing I", p);
				return;
			}
			utils.removeLore("&7Piercing I", p);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("armourpiercer", 2))) {
			if (utils.toggleState(p, "armourpiercer", 2)) {
				utils.applyLore("&7Piercing II", p);
				return;
			}
			utils.removeLore("&7Piercing II", p);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("armourpiercer", 3))) {
			if (utils.toggleState(p, "armourpiercer", 3)) {
				utils.applyLore("&7Piercing III", p);
				return;
			}
			utils.removeLore("&7Piercing III", p);
			return;
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		
		Player p = e.getPlayer();
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (e.getItemDrop().getItemStack().getItemMeta().getLore() == null) return;
		
		if (e.getItemDrop().getItemStack().getItemMeta().getLore().toString().contains(utils.color("&7Piercing"))) {
			e.setCancelled(true);
			return;
		}
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("armourpiercer")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("armourpiercer", 1))) {
					utils.toggleState(p, "armourpiercer", 1);
					utils.removeLore("&7Piercing I", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("armourpiercer", 2))) {
					utils.toggleState(p, "armourpiercer", 2);
					utils.removeLore("&7Piercing II", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("armourpiercer", 3))) {
					utils.toggleState(p, "armourpiercer", 3);
					utils.removeLore("&7Piercing III", p);
					return;
				}
			}
			
		}
	}
	
	@EventHandler
	public void onItemMove(InventoryClickEvent e) {
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		Player p = (Player) e.getWhoClicked();
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
			if (utils.hotbarCheck(p, e, "armourpiercer", "&7Piercing I", 1)) return;
			if (utils.hotbarCheck(p, e, "armourpiercer", "&7Piercing II", 2)) return;
			if (utils.hotbarCheck(p, e, "armourpiercer", "&7Piercing III", 3)) return;
			return;
		}
		
		if (e.getCurrentItem().getItemMeta().getLore() == null) return;
		
		if (e.getCurrentItem().getItemMeta().getLore().toString().contains(utils.color("&7Piercing"))) {
			e.setCancelled(true);
			return;
		}
		
		if (e.getAction() == InventoryAction.HOTBAR_MOVE_AND_READD) {
			if (p.getInventory().getItem(e.getHotbarButton()).getItemMeta().getLore() != null) {
				if (p.getInventory().getItem(e.getHotbarButton()).getItemMeta().getLore().contains(utils.color("&7Piercing"))) {
					e.setCancelled(true);
					return;
				}
			}
		}
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (utils.hotbarCheck(p, e, "armourpiercer", "&7Piercing I", 1)) return;
		if (utils.hotbarCheck(p, e, "armourpiercer", "&7Piercing II", 2)) return;
		if (utils.hotbarCheck(p, e, "armourpiercer", "&7Piercing III", 3)) return;
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("armourpiercer")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("armourpiercer", 1)) || e.getCursor().isSimilar(utils.giveItem("armourpiercer", 1))) {
					utils.toggleState(p, "armourpiercer", 1);
					utils.removeLore("&7Piercing I", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("armourpiercer", 2)) || e.getCursor().isSimilar(utils.giveItem("armourpiercer", 2))) {
					utils.toggleState(p, "armourpiercer", 2);
					utils.removeLore("&7Piercing II", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("armourpiercer", 3)) || e.getCursor().isSimilar(utils.giveItem("armourpiercer", 3))) {
					utils.toggleState(p, "armourpiercer", 3);
					utils.removeLore("&7Piercing III", p);
					return;
				}
			}
			
		}
	}
	
}
