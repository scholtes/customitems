package me.scholtes.customitems.effects;

import java.io.File;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.scholtes.customitems.Utils;

public class ArmourUpgrade implements Listener {
	
	private Utils utils;

	public ArmourUpgrade(Utils utils) {
		this.utils = utils;
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!p.isSneaking()) {
			return;
		}
		if (e.getAction() != Action.LEFT_CLICK_AIR) {
			return;
		}
		if (p.getInventory().getItem(8) == null) {
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("armourupgrade", 1))) {
			if (utils.toggleState(p, "armourupgrade", 1)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 999999999, 0));
				return;
			}
			p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("armourupgrade", 2))) {
			if (utils.toggleState(p, "armourupgrade", 2)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 999999999, 1));
				return;
			}
			p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
			return;
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		
		Player p = e.getPlayer();
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("armourupgrade")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("armourupgrade", 1))) {
					utils.toggleState(p, "armourupgrade", 1);
					p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("armourupgrade", 2))) {
					utils.toggleState(p, "armourupgrade", 2);
					p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
					return;
				}
			}
			
		}
	}
	
	@EventHandler
	public void onItemMove(InventoryClickEvent e) {
		
		Player p = (Player) e.getWhoClicked();
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
			if (utils.hotbarCheck2(p, e, "armourupgrade", 1)) return;
			if (utils.hotbarCheck2(p, e, "armourupgrade", 2)) return;
			return;
		}
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (utils.hotbarCheck2(p, e, "armourupgrade", 1)) return;
		if (utils.hotbarCheck2(p, e, "armourupgrade", 2)) return;
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("armourupgrade")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("armourupgrade", 1)) || e.getCursor().isSimilar(utils.giveItem("armourupgrade", 1))) {
					utils.toggleState(p, "armourupgrade", 1);
					p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("armourupgrade", 2)) || e.getCursor().isSimilar(utils.giveItem("armourupgrade", 1))) {
					utils.toggleState(p, "armourupgrade", 2);
					p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
					return;
				}
			}
			
			
		}
	}
	
}
