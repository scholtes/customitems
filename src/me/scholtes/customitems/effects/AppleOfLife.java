package me.scholtes.customitems.effects;

import java.io.File;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.scholtes.customitems.Utils;

public class AppleOfLife implements Listener {
	
	private Utils utils;

	public AppleOfLife(Utils utils) {
		this.utils = utils;
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!p.isSneaking()) {
			return;
		}
		if (e.getAction() != Action.LEFT_CLICK_AIR) {
			return;
		}
		if (p.getInventory().getItem(8) == null) {
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("appleoflife", 1))) {
			if (utils.toggleState(p, "appleoflife", 1)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999999, 0));
				return;
			}
			p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("appleoflife", 2))) {
			if (utils.toggleState(p, "appleoflife", 2)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999999, 1));
				return;
			}
			p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("appleoflife", 3))) {
			if (utils.toggleState(p, "appleoflife", 3)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999999, 2));
				return;
			}
			p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("appleoflife", 4))) {
			if (utils.toggleState(p, "appleoflife", 4)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999999, 3));
				return;
			}
			p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("appleoflife", 5))) {
			if (utils.toggleState(p, "appleoflife", 5)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999999, 4));
				return;
			}
			p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
			return;
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		
		Player p = e.getPlayer();
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("appleoflife")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("appleoflife", 1))) {
					utils.toggleState(p, "appleoflife", 1);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("appleoflife", 2))) {
					utils.toggleState(p, "appleoflife", 2);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("appleoflife", 3))) {
					utils.toggleState(p, "appleoflife", 3);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 4) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("appleoflife", 4))) {
					utils.toggleState(p, "appleoflife", 4);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 5) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("appleoflife", 5))) {
					utils.toggleState(p, "appleoflife", 5);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
		}
	}
	
	@EventHandler
	public void onItemMove(InventoryClickEvent e) {
		
		Player p = (Player) e.getWhoClicked();
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
			if (utils.hotbarCheck2(p, e, "appleoflife", 1)) return;
			if (utils.hotbarCheck2(p, e, "appleoflife", 2)) return;
			if (utils.hotbarCheck2(p, e, "appleoflife", 3)) return;
			if (utils.hotbarCheck2(p, e, "appleoflife", 4)) return;
			if (utils.hotbarCheck2(p, e, "appleoflife", 5)) return;
			return;
		}
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (utils.hotbarCheck2(p, e, "appleoflife", 1)) return;
		if (utils.hotbarCheck2(p, e, "appleoflife", 2)) return;
		if (utils.hotbarCheck2(p, e, "appleoflife", 3)) return;
		if (utils.hotbarCheck2(p, e, "appleoflife", 4)) return;
		if (utils.hotbarCheck2(p, e, "appleoflife", 5)) return;
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("appleoflife")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("appleoflife", 1)) || e.getCursor().isSimilar(utils.giveItem("appleoflife", 1))) {
					utils.toggleState(p, "appleoflife", 1);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("appleoflife", 2)) || e.getCursor().isSimilar(utils.giveItem("appleoflife", 2))) {
					utils.toggleState(p, "appleoflife", 2);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("appleoflife", 3)) || e.getCursor().isSimilar(utils.giveItem("appleoflife", 3))) {
					utils.toggleState(p, "appleoflife", 3);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 4) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("appleoflife", 4)) || e.getCursor().isSimilar(utils.giveItem("appleoflife", 4))) {
					utils.toggleState(p, "appleoflife", 4);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 5) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("appleoflife", 5)) || e.getCursor().isSimilar(utils.giveItem("appleoflife", 5))) {
					utils.toggleState(p, "appleoflife", 5);
					p.removePotionEffect(PotionEffectType.HEALTH_BOOST);
					return;
				}
			}
			
		}
	}
	
	@EventHandler
	public void onEat(PlayerItemConsumeEvent e) {
		if (e.getItem().getItemMeta().getDisplayName() == null) return;
		if (e.getItem().getItemMeta().getDisplayName().contains(utils.color("&eApple of Life"))) {
			e.setCancelled(true);
		}
	}
	
}
