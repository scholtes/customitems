package me.scholtes.customitems.effects;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.scholtes.customitems.CustomItems;
import me.scholtes.customitems.Utils;

public class RandomItem implements Listener {
	
	private Utils utils;
	private CustomItems plugin;

	public RandomItem(Utils utils, CustomItems plugin) {
		this.utils = utils;
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		
		if (p.getInventory().getItemInMainHand() == null) return;
		
		ItemStack mainHand = p.getInventory().getItemInMainHand();
		
		if (mainHand.getType() != Material.CHEST) return;		
		if (mainHand.getItemMeta().getDisplayName() == null) return;		
		if (!mainHand.isSimilar(utils.giveItem("randomitem", 1))) return;
		
		File data = new File("plugins/CustomItems/", "itemnames.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		List<String> items = cfg.getStringList("items");
		Collections.shuffle(items);
		String[] part = items.get(0).split(";");
		if (part[0].equals("randomitem")) {
			part = items.get(1).split(";");
		}
		Random rand = new Random();
		int random = rand.nextInt(Integer.parseInt(part[1]) - 1 + 1) + 1;
		p.getInventory().addItem(utils.giveItem(part[0], random));
		p.sendMessage(utils.color("&6Item &8> &fYou have received a random custom item!"));
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				p.getInventory().removeItem(utils.giveItem("randomitem", 1));
			}
		}, 1L);
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		if (e.getBlock().getType() != Material.CHEST) return;
		ItemStack mainHand = e.getItemInHand();
		if (mainHand.getItemMeta().getDisplayName() == null) return;		
		if (!mainHand.isSimilar(utils.giveItem("randomitem", 1))) return;
		e.setCancelled(true);
	}

}
