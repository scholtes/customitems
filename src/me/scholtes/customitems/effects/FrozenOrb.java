package me.scholtes.customitems.effects;

import java.io.File;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.scholtes.customitems.Utils;

public class FrozenOrb implements Listener {
	
	private Utils utils;

	public FrozenOrb(Utils utils) {
		this.utils = utils;
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!p.isSneaking()) {
			return;
		}
		if (e.getAction() != Action.LEFT_CLICK_AIR) {
			return;
		}
		if (p.getInventory().getItem(8) == null) {
			return;
		}
		
		if (p.getInventory().getChestplate() == null) return;
		
		ItemStack chestplate = p.getInventory().getChestplate();
		
		if (chestplate.getType() != Material.DIAMOND_CHESTPLATE &&
			chestplate.getType() != Material.GOLD_CHESTPLATE &&
			chestplate.getType() != Material.IRON_CHESTPLATE &&
			chestplate.getType() != Material.LEATHER_CHESTPLATE &&
			chestplate.getType() != Material.CHAINMAIL_CHESTPLATE) return;
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("frozenorb", 1))) {
			if (utils.toggleState(p, "frozenorb", 1)) {
				utils.applyLoreChest("&7Frozen I", p);
				return;
			}
			utils.removeLoreChest("&7Frozen I", p);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("frozenorb", 2))) {
			if (utils.toggleState(p, "frozenorb", 2)) {
				utils.applyLoreChest("&7Frozen II", p);
				return;
			}
			utils.removeLoreChest("&7Frozen II", p);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("frozenorb", 3))) {
			if (utils.toggleState(p, "frozenorb", 3)) {
				utils.applyLoreChest("&7Frozen III", p);
				return;
			}
			utils.removeLoreChest("&7Frozen III", p);
			return;
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		
		Player p = e.getPlayer();
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (e.getItemDrop().getItemStack().getItemMeta().getLore() == null) return;
		
		if (e.getItemDrop().getItemStack().getItemMeta().getLore().toString().contains(utils.color("&7Frozen"))) {
			e.setCancelled(true);
			return;
		}
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("frozenorb")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("frozenorb", 1))) {
					utils.toggleState(p, "frozenorb", 1);
					utils.removeLoreChest("&7Frozen I", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("frozenorb", 2))) {
					utils.toggleState(p, "frozenorb", 2);
					utils.removeLoreChest("&7Frozen II", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("frozenorb", 3))) {
					utils.toggleState(p, "frozenorb", 3);
					utils.removeLoreChest("&7Frozen III", p);
					return;
				}
			}
			
		}
	}
	
	@EventHandler
	public void onItemMove(InventoryClickEvent e) {
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		Player p = (Player) e.getWhoClicked();
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
			if (utils.hotbarCheck(p, e, "frozenorb", "&7Frozen I", 1)) return;
			if (utils.hotbarCheck(p, e, "frozenorb", "&7Frozen II", 2)) return;
			if (utils.hotbarCheck(p, e, "frozenorb", "&7Frozen III", 3)) return;
			return;
		}
		
		if (e.getCurrentItem().getItemMeta().getLore() == null) return;
		
		if (e.getCurrentItem().getItemMeta().getLore().toString().contains(utils.color("&7Frozen"))) {
			e.setCancelled(true);
			return;
		}
		
		if (e.getAction() == InventoryAction.HOTBAR_MOVE_AND_READD) {
			if (p.getInventory().getItem(e.getHotbarButton()).getItemMeta().getLore() != null) {
				if (p.getInventory().getItem(e.getHotbarButton()).getItemMeta().getLore().contains(utils.color("&7Frozen"))) {
					e.setCancelled(true);
					return;
				}
			}
		}
		
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (utils.hotbarCheck(p, e, "frozenorb", "&7Frozen I", 1)) return;
		if (utils.hotbarCheck(p, e, "frozenorb", "&7Frozen II", 2)) return;
		if (utils.hotbarCheck(p, e, "frozenorb", "&7Frozen III", 3)) return;
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("frozenorb")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("frozenorb", 1)) || e.getCursor().isSimilar(utils.giveItem("frozenorb", 1))) {
					utils.toggleState(p, "frozenorb", 1);
					utils.removeLoreChest("&7Frozen I", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("frozenorb", 2)) || e.getCursor().isSimilar(utils.giveItem("frozenorb", 2))) {
					utils.toggleState(p, "frozenorb", 2);
					utils.removeLoreChest("&7Frozen II", p);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("frozenorb", 3)) || e.getCursor().isSimilar(utils.giveItem("frozenorb", 3))) {
					utils.toggleState(p, "frozenorb", 3);
					utils.removeLoreChest("&7Frozen III", p);
					return;
				}
			}
			
		}
	}
	
}
