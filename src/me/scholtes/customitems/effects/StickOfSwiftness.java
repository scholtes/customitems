package me.scholtes.customitems.effects;

import java.io.File;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.scholtes.customitems.Utils;

public class StickOfSwiftness implements Listener {
	
	private Utils utils;

	public StickOfSwiftness(Utils utils) {
		this.utils = utils;
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!p.isSneaking()) {
			return;
		}
		if (e.getAction() != Action.LEFT_CLICK_AIR) {
			return;
		}
		if (p.getInventory().getItem(8) == null) {
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("stickofswiftness", 1))) {
			e.setCancelled(true);
			if (utils.toggleState(p, "stickofswiftness", 1)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 999999999, 0));
				return;
			}
			p.removePotionEffect(PotionEffectType.FAST_DIGGING);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("stickofswiftness", 2))) {
			if (utils.toggleState(p, "stickofswiftness", 2)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 999999999, 1));
				return;
			}
			p.removePotionEffect(PotionEffectType.FAST_DIGGING);
			return;
		}
		
		if (p.getInventory().getItem(8).isSimilar(utils.giveItem("stickofswiftness", 3))) {
			if (utils.toggleState(p, "stickofswiftness", 3)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 999999999, 2));
				return;
			}
			p.removePotionEffect(PotionEffectType.FAST_DIGGING);
			return;
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		
		Player p = e.getPlayer();
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("stickofswiftness")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("stickofswiftness", 1))) {
					utils.toggleState(p, "stickofswiftness", 1);
					p.removePotionEffect(PotionEffectType.FAST_DIGGING);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("stickofswiftness", 2))) {
					utils.toggleState(p, "stickofswiftness", 2);
					p.removePotionEffect(PotionEffectType.FAST_DIGGING);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getItemDrop().getItemStack().isSimilar(utils.giveItem("stickofswiftness", 3))) {
					utils.toggleState(p, "stickofswiftness", 3);
					p.removePotionEffect(PotionEffectType.FAST_DIGGING);
					return;
				}
			}
			
		}
	}
	
	@EventHandler
	public void onItemMove(InventoryClickEvent e) {
		
		Player p = (Player) e.getWhoClicked();
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
			if (utils.hotbarCheck2(p, e, "stickofswiftness", 1)) return;
			if (utils.hotbarCheck2(p, e, "stickofswiftness", 2)) return;
			if (utils.hotbarCheck2(p, e, "stickofswiftness", 3)) return;
			return;
		}
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!data.exists()) {
			return;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".state") == null) {
			return;
		}
		if (cfg.getBoolean("data." + p.getUniqueId() + ".state") == false) {
			return;
		}
		
		if (utils.hotbarCheck2(p, e, "stickofswiftness", 1)) return;
		if (utils.hotbarCheck2(p, e, "stickofswiftness", 2)) return;
		if (utils.hotbarCheck2(p, e, "stickofswiftness", 3)) return;
		
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals("stickofswiftness")) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 1) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("stickofswiftness", 1)) || e.getCursor().isSimilar(utils.giveItem("stickofswiftness", 1))) {
					utils.toggleState(p, "stickofswiftness", 1);
					p.removePotionEffect(PotionEffectType.FAST_DIGGING);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 2) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("stickofswiftness", 2)) || e.getCursor().isSimilar(utils.giveItem("stickofswiftness", 2))) {
					utils.toggleState(p, "stickofswiftness", 2);
					p.removePotionEffect(PotionEffectType.FAST_DIGGING);
					return;
				}
			}
			
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == 3) {
				if (e.getCurrentItem().isSimilar(utils.giveItem("stickofswiftness", 3)) || e.getCursor().isSimilar(utils.giveItem("stickofswiftness", 3))) {
					utils.toggleState(p, "stickofswiftness", 3);
					p.removePotionEffect(PotionEffectType.FAST_DIGGING);
					return;
				}
			}
			
			
		}
	}
	
}
