package me.scholtes.customitems;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

public class Utils {
	
	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}

	public ItemStack giveItem(String text, int tier) {
		
		
		// Apple Of Life
		if (text.equals("appleoflife")) {
			ItemStack item = new ItemStack(Material.APPLE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&eApple of Life &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Gives +2 hearts when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Gives +4 hearts when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Gives +6 hearts when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Gives +8 hearts when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Gives +10 hearts when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}	
		
		
		// Armour Piercer
		if (text.equals("armourpiercer")) {
			ItemStack item = new ItemStack(Material.ARROW);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&2Armour Piercer &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Piercing I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Piercing II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Piercing III when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a sword"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Armour Upgrade
		if (text.equals("armourupgrade")) {
			ItemStack item = new ItemStack(Material.IRON_INGOT);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&3Armour Upgrade &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Resistance I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Resistance II when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Backup Plan
		if (text.equals("backupplan")) {
			ItemStack item = new ItemStack(Material.WEB);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&fBackup Plan &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies QuickTrap I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies QuickTrap II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies QuickTrap III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies QuickTrap IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies QuickTrap V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a sword"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Bandage
		if (text.equals("bandage")) {
			ItemStack item = new ItemStack(Material.PAPER);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&cBandage &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Regeneration I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Regeneration II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Regeneration III when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Charge Up
		if (text.equals("chargeup")) {
			ItemStack item = new ItemStack(Material.EMERALD);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&2Charge Up &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Ramming I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Ramming II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Ramming III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Ramming IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Ramming V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a sword"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Diamond of Rage
		if (text.equals("diamondofrage")) {
			ItemStack item = new ItemStack(Material.DIAMOND);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&cDiamond of Rage &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Rage I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Rage II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Rage III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Rage IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Rage V when activated"));
			}
			if (tier == 6) {
				lore.add(color("&6Applies Rage VI when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a sword"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Drilling Machine
		if (text.equals("drillingmachine")) {
			ItemStack item = new ItemStack(Material.HOPPER);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&9Drilling Machine &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Drill I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Drill II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Drill III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Drill IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Drill V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a pickaxe"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Explosive
		if (text.equals("explosive")) {
			ItemStack item = new ItemStack(Material.SULPHUR);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&4Explosive &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Explosive I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Explosive II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Explosive III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Explosive IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Explosive V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a pickaxe"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Frozen Orb
		if (text.equals("frozenorb")) {
			ItemStack item = new ItemStack(Material.INK_SACK, 1, (short) 6);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&bFrozen Orb &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Frozen I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Frozen II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Frozen III when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Heavy Weights
		if (text.equals("heavyweights")) {
			ItemStack item = new ItemStack(Material.ANVIL);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&3Heavy Weights &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies AntiKnockback I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies AntiKnockback II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies AntiKnockback III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies AntiKnockback IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies AntiKnockback V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Lightweight Feather
		if (text.equals("lightweightfeather")) {
			ItemStack item = new ItemStack(Material.FEATHER);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&dLightweight Feather &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Speed I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Speed II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Speed III when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
			
		
		// Magic Hat
		if (text.equals("magicwand")) {
			ItemStack item = new ItemStack(Material.BLAZE_ROD);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&3Magic Wand &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Trickster I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Trickster II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Trickster III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Trickster IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Trickster V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
			
		// Sharpener
		if (text.equals("sharpener")) {
			ItemStack item = new ItemStack(Material.CLAY_BRICK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&7Sharpener &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Strength I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Strength II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Strength III when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Smoke Bomb
		if (text.equals("smokebomb")) {
			ItemStack item = new ItemStack(Material.ENDER_PEARL);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&7Smoke Bomb &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Ninja I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Ninja II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Ninja III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Ninja IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Ninja V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a sword"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
			
			
		// Spring
		if (text.equals("spring")) {
			ItemStack item = new ItemStack(Material.SLIME_BALL);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&fSpring &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Jump Boost I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Jump Boost II when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
			
			
		// Stick of Swiftness
		if (text.equals("stickofswiftness")) {
			ItemStack item = new ItemStack(Material.STICK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&bStick of Swiftness &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Haste I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Haste II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Haste III when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		// Sword Skills
		if (text.equals("swordskills")) {
			ItemStack item = new ItemStack(Material.BOOK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&bSword Skills &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Deathbringer I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Deathbringer II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Deathbringer III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Deathbringer IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Deathbringer V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a sword"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		// Toxic Water
		if (text.equals("toxicwater")) {
			ItemStack item = new ItemStack(Material.INK_SACK, 1, (short) 5);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&5Toxic Water &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Toxic I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Toxic II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Toxic III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Toxic IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Toxic V when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		// Vampire Fangs
		if (text.equals("vampirefangs")) {
			ItemStack item = new ItemStack(Material.REDSTONE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&4Vampire Fangs &8- &cTier " + tier));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			if (tier == 1) {
				lore.add(color("&6Applies Vampire I when activated"));
			}
			if (tier == 2) {
				lore.add(color("&6Applies Vampire II when activated"));
			}
			if (tier == 3) {
				lore.add(color("&6Applies Vampire III when activated"));
			}
			if (tier == 4) {
				lore.add(color("&6Applies Vampire IV when activated"));
			}
			if (tier == 5) {
				lore.add(color("&6Applies Vampire V when activated"));
			}
			if (tier == 6) {
				lore.add(color("&6Applies Vampire VI when activated"));
			}
			lore.add("");
			lore.add(color("&ePut this in the last slot"));
			lore.add(color("&eof your hotbar"));
			lore.add("");
			lore.add(color("&e&nShift + Left Click &e&l&nair&e&n to toggle"));
			lore.add(color("&e&nwhile holding a sword"));
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		// Vampire Fangs
		if (text.equals("randomitem")) {
			ItemStack item = new ItemStack(Material.CHEST, tier);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(color("&6Random Custom Item"));
			List<String> lore = new ArrayList<String>();
			lore.add("");
			lore.add(color("&eThis will give you a"));
			lore.add(color("&erandom custom item"));
			lore.add("");
			lore.add(color("&e&nClick to open"));
			lore.add("");
			meta.setLore(lore);
			item.setItemMeta(meta);
			
			return item;
		}
		
		
		return null;
	}
	
	public void applyLore(String text, Player p) {
		ItemStack mainHand = p.getInventory().getItemInMainHand();
		ItemMeta meta = mainHand.getItemMeta();
		List<String> oldLore = meta.getLore();
		if (oldLore == null) {
			List<String> empty = new ArrayList<String>();
			oldLore = empty;
		}
		oldLore.add(color(text));
		meta.setLore(oldLore);
		mainHand.setItemMeta(meta);
	}
	
	public void removeLore(String text, Player p) {
		for (ItemStack i : p.getInventory().getContents()) {
			if (i == null || i.getType() == Material.AIR) return;
			if (i.getItemMeta().getLore() == null || i.getItemMeta().getLore().isEmpty()) return;
			ItemMeta meta = i.getItemMeta();
			List<String> oldLore = meta.getLore();
			if (oldLore.contains(color(text))) {
				oldLore.remove(color(text));
			}
			meta.setLore(oldLore);
			i.setItemMeta(meta);
		}
	}
	
	public void applyLoreChest(String text, Player p) {
		ItemStack chestplate = p.getInventory().getChestplate();
		ItemMeta meta = chestplate.getItemMeta();
		List<String> oldLore = meta.getLore();
		if (oldLore == null) {
			List<String> empty = new ArrayList<String>();
			oldLore = empty;
		}
		oldLore.add(color(text));
		meta.setLore(oldLore);
		chestplate.setItemMeta(meta);
	}
	
	public void removeLoreChest(String text, Player p) {
		ItemStack chestplate = p.getInventory().getChestplate();
		ItemMeta meta = chestplate.getItemMeta();
		List<String> oldLore = chestplate.getItemMeta().getLore();
		oldLore.remove(color(text));
		meta.setLore(oldLore);
		chestplate.setItemMeta(meta);
	}
	
	public boolean hotbarCheck(Player p, InventoryClickEvent e, String name, String enchant, int tier) {
		if (e.getHotbarButton() != 8) {
			return false;
		}
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals(name)) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == tier) {
				if (p.getInventory().getItem(8).isSimilar(giveItem(name, tier))) {
					toggleState(p, name, 1);
					removeLore(enchant, p);
					return true;
				}
			}
			
		}
		return false;
	}
	
	public boolean hotbarCheck2(Player p, InventoryClickEvent e, String name, int tier) {
		if (e.getHotbarButton() != 8) {
			return false;
		}
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		PotionEffectType potion = PotionEffectType.HEAL;
		if (name.equals("appleoflife")) {
			potion = PotionEffectType.HEALTH_BOOST;
		}
		if (name.equals("armourupgrade")) {
			potion = PotionEffectType.DAMAGE_RESISTANCE;
		}
		if (name.equals("bandage")) {
			potion = PotionEffectType.REGENERATION;
		}
		if (name.equals("lightweightfeather")) {
			potion = PotionEffectType.SPEED;
		}
		if (name.equals("sharpener")) {
			potion = PotionEffectType.INCREASE_DAMAGE;
		}
		if (name.equals("spring")) {
			potion = PotionEffectType.JUMP;
		}
		if (name.equals("stickofswiftness")) {
			potion = PotionEffectType.FAST_DIGGING;
		}
		if (cfg.getString("data." + p.getUniqueId() + ".item").equals(name)) {
			if (cfg.getInt("data." + p.getUniqueId() + ".tier") == tier) {
				if (p.getInventory().getItem(8).isSimilar(giveItem(name, tier))) {
					toggleState(p, name, 1);
					p.removePotionEffect(potion);
					return true;
				}
			}
			
		}
		return false;
	}
	
	public boolean toggleState(Player player, String item, int tier) {
		
		File data = new File("plugins/CustomItems/", "playerdata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (data.exists()) {
			if (cfg.getString("data." + player.getUniqueId() + ".state") == null) {
				cfg.set("data." + player.getUniqueId() + ".state", true);
				cfg.set("data." + player.getUniqueId() + ".item", item);
				cfg.set("data." + player.getUniqueId() + ".tier", tier);
				player.sendMessage(color("&6Item &8> &fYou have toggled the custom effect &aon&f!"));
				try {
					cfg.save(data);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				return true;
				
			} else if (cfg.getBoolean("data." + player.getUniqueId() + ".state") == false) {
				cfg.set("data." + player.getUniqueId() + ".state", true);
				cfg.set("data." + player.getUniqueId() + ".item", item);
				cfg.set("data." + player.getUniqueId() + ".tier", tier);
				player.sendMessage(color("&6Item &8> &fYou have toggled the custom effect &aon&f!"));
				try {
					cfg.save(data);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				return true;
				
			} else if (cfg.getBoolean("data." + player.getUniqueId() + ".state") == true) {
				cfg.set("data." + player.getUniqueId() + ".state", false);
				cfg.set("data." + player.getUniqueId() + ".item", null);
				cfg.set("data." + player.getUniqueId() + ".tier", null);
				player.sendMessage(color("&6Item &8> &fYou have toggled the custom effect &coff&f!"));
				try {
					cfg.save(data);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				return false;
				
			}
			
		} else if (!data.exists()) {
			cfg.set("data." + player.getUniqueId() + ".state", true);
			cfg.set("data." + player.getUniqueId() + ".item", item);
			cfg.set("data." + player.getUniqueId() + ".tier", tier);
			player.sendMessage(color("&6Item &8> &fYou have toggled the custom effect &aon&f!"));
			try {
				cfg.save(data);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return true;
			
		}
		return false;
	}
	
}
