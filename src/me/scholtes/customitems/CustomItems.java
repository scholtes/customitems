package me.scholtes.customitems;

import org.bukkit.plugin.java.JavaPlugin;

import me.scholtes.customitems.commands.GiveCommand;
import me.scholtes.customitems.commands.TabCompletion;
import me.scholtes.customitems.effects.AppleOfLife;
import me.scholtes.customitems.effects.ArmourPiercer;
import me.scholtes.customitems.effects.ArmourUpgrade;
import me.scholtes.customitems.effects.BackupPlan;
import me.scholtes.customitems.effects.Bandage;
import me.scholtes.customitems.effects.ChargeUp;
import me.scholtes.customitems.effects.DiamondOfRage;
import me.scholtes.customitems.effects.DrillingMachine;
import me.scholtes.customitems.effects.Explosive;
import me.scholtes.customitems.effects.FrozenOrb;
import me.scholtes.customitems.effects.HeavyWeights;
import me.scholtes.customitems.effects.LightweightFeather;
import me.scholtes.customitems.effects.MagicWand;
import me.scholtes.customitems.effects.RandomItem;
import me.scholtes.customitems.effects.Sharpener;
import me.scholtes.customitems.effects.SmokeBomb;
import me.scholtes.customitems.effects.Spring;
import me.scholtes.customitems.effects.StickOfSwiftness;
import me.scholtes.customitems.effects.SwordSkills;
import me.scholtes.customitems.effects.ToxicWater;
import me.scholtes.customitems.effects.VampireFangs;

public class CustomItems extends JavaPlugin {
	
	Utils utils = new Utils();
	
	public void onEnable() {
		
		getServer().getPluginManager().registerEvents(new AppleOfLife(utils), this);
		getServer().getPluginManager().registerEvents(new ArmourUpgrade(utils), this);
		getServer().getPluginManager().registerEvents(new ArmourPiercer(utils), this);
		getServer().getPluginManager().registerEvents(new BackupPlan(utils), this);
		getServer().getPluginManager().registerEvents(new Bandage(utils), this);
		getServer().getPluginManager().registerEvents(new ChargeUp(utils), this);
		getServer().getPluginManager().registerEvents(new DiamondOfRage(utils), this);
		getServer().getPluginManager().registerEvents(new DrillingMachine(utils), this);
		getServer().getPluginManager().registerEvents(new Explosive(utils), this);
		getServer().getPluginManager().registerEvents(new FrozenOrb(utils), this);
		getServer().getPluginManager().registerEvents(new HeavyWeights(utils), this);
		getServer().getPluginManager().registerEvents(new LightweightFeather(utils), this);
		getServer().getPluginManager().registerEvents(new MagicWand(utils), this);
		getServer().getPluginManager().registerEvents(new Sharpener(utils), this);
		getServer().getPluginManager().registerEvents(new SmokeBomb(utils), this);
		getServer().getPluginManager().registerEvents(new Spring(utils), this);
		getServer().getPluginManager().registerEvents(new StickOfSwiftness(utils), this);
		getServer().getPluginManager().registerEvents(new VampireFangs(utils), this);
		getServer().getPluginManager().registerEvents(new ToxicWater(utils), this);
		getServer().getPluginManager().registerEvents(new SwordSkills(utils), this);
		getServer().getPluginManager().registerEvents(new RandomItem(utils, this), this);
		
		getCommand("customitem").setExecutor(new GiveCommand(utils));
		getCommand("customitem").setTabCompleter(new TabCompletion());
		
	}

}
